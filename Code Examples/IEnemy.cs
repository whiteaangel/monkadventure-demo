
using UnityEngine;

namespace Enemy
{
    public interface IEnemy
    {
        void Attack();
        void LookAtPlayer(Transform player);
        public void TakeDamage(int damage);
        void Die();
        void Resurrect();
    }
}
using System.Collections;
using Enemy.ScriptableObject;
using UnityEngine;

namespace Enemy
{
    public class EnemyStatic : MonoBehaviour, IEnemy
    {
        public Animator deathAnimator;
        public Animator animator;
        public new BoxCollider2D collider;
        public LayerMask playerLayer;
        public GameObject enemySprite;
        public GameObject enemyLight;
        public GameObject projectilePrefab;
        public Transform firePoint;
        public Transform findPoint;
        public EnemySettings enemySettings;
        public bool haveAnimation;
        public bool isFlying;

        private int _currentHealth;
        private int _essenceCount;
        private string _animName;
        protected int Damage;

        protected float FindRange;
        protected float FireRate;
        protected float NextAttackTime = 0f;
    
        private bool _isFindPlayer = false;
        private bool _isFlipped = false;
        protected bool IsDead = false;

    
        private void Awake()
        {
            SetValue();
        }
    
        public virtual void Update()
        {
            if(IsDead)
                return;
        
            //Ищем игрока, если нашли то атакуем его
            FindPlayer();
            if(_isFindPlayer)
                Attack();
        }
    
        //Загружаем параметры врага из файла настроек
        private void SetValue()
        {
            _currentHealth = enemySettings.currentHealth;
            _essenceCount = enemySettings.essenceCount;
            _animName = enemySettings.animName;
            FireRate = enemySettings.fireRate;
            FindRange = enemySettings.findRange;
            Damage = enemySettings.damage;
        }

        //Атакуем врага через заданный промежуток времени
        public virtual void Attack()
        {
            if (!(Time.time > NextAttackTime)) return;
            StartCoroutine(Shoot());
            animator.SetTrigger("Attack");
            NextAttackTime = Time.time + FireRate;
        }

        private IEnumerator Shoot()
        {
            yield return new WaitForSeconds(0.2f);
            Instantiate(projectilePrefab, firePoint.position, firePoint.rotation);
        }
    
        //Функция для поиска игрока в заданной области
        private void FindPlayer()
        {
            Collider2D hitPlayer = Physics2D.OverlapCircle(findPoint.position, FindRange, playerLayer);
            if(hitPlayer == null )
            {
                _isFindPlayer = false;
                return;
            }
            _isFindPlayer = true;
            LookAtPlayer(hitPlayer.transform);
        }

        //Поворачивает противника в сторону игрока
        public void LookAtPlayer(Transform player)
        {
            Vector3 flipped = transform.localScale;
            flipped.z = -1f;

            if (transform.position.x > player.position.x && _isFlipped)
            {
                transform.localScale = flipped;
                transform.Rotate(0f,180f,0f);
                _isFlipped = false;
            }
            else if (transform.position.x < player.position.x && !_isFlipped)
            {
                transform.localScale = flipped;
                transform.Rotate(0f,180f,0f);
                _isFlipped = true;
            }
        }

        //Функция вызывающяяся при попадании атаки игрока по противнику
        public void TakeDamage(int damage)
        {
            AudioClip clip = Resources.Load("Sound/hurtEnemy") as AudioClip;
            Managers.Managers.Audio.PlaySound(clip);
            _currentHealth -= damage;
            if (_currentHealth <= 0)
            {
                Die();
            }
        }
        public void Die()
        {
            collider.enabled = false;
            if(!isFlying)
                GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
            StopAllCoroutines();
        
            AudioClip clip = Resources.Load("Sound/fire") as AudioClip;
            Managers.Managers.Audio.PlaySound(clip);
        
            enemySprite.SetActive(false);
            if(!haveAnimation)
                deathAnimator.SetTrigger(_animName);
            else
            {
                animator.SetTrigger("Death");
            }
        
            Managers.Managers.Player.essence += _essenceCount;
            IsDead = true;
        
            if(enemyLight == null)
                return;
            enemyLight.SetActive(false);
        }

        public void Resurrect()
        {
            if(!IsDead)
                return;
        
            enemySprite.SetActive(true);
        
            if(!isFlying)
                GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        
            collider.enabled = true;
            IsDead = false;
            _isFindPlayer = false;

            if (haveAnimation)
            {
                animator.SetTrigger("Resurrect");
            }

            if(enemyLight == null)
                return;
            enemyLight.SetActive(true);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Items;
using Player;
using UI;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Managers
{
    public class InventoryManager : MonoBehaviour, IGameManager
    {
        public ManagerStatus status {get; private set;}
    
        //Проверяем, открыт или закрыт инвентарь
        public bool isInventoryOpen { get; set; }
        public bool isSlotHighlighted { get; set; }
        public int currentSlot { get; set; }
        public string currentItem { get; set; }

        //Name, count. Словарь в котором находятся предметы, которые есть у игрока сейчас и их количеcтво
        public Dictionary<string, int> Items = new Dictionary<string, int>();
        //Slot number, item name. Словарь, который показывает какие предметы у игрока сейчас находятся в быстрых слотах
        public Dictionary<int, string> SlotItem = new Dictionary<int, string>();
    
        //Список в котором будут находиться все предметы в игре и их эффекты, должен заполняться при старте игры из xml
        private readonly List<Item> _items = new List<Item>();

        public void Startup()
        {
            isInventoryOpen = false;
            isSlotHighlighted = false;
        
            //Загружаем список всех предметов из xml
            LoadItemListFromXml();

            Managers.Data.LoadPlayerInventory();
        
            status = ManagerStatus.Started;
        }

        private void LoadItemListFromXml()
        {
            TextAsset xmlTextAsset = Resources.Load<TextAsset>("Xml/Items");
        
            XmlDocument  xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(xmlTextAsset.text);
        
            for (var i = 0; i < xmlDocument.SelectNodes("items/item").Count; i++)
            {
                var item = new Item
                {
                    name = xmlDocument.SelectNodes("items/item/name")?[i].InnerXml,
                    itemType = xmlDocument.SelectNodes("items/item/type")?[i].InnerXml,
                    isConsumable = Convert.ToBoolean(xmlDocument.SelectNodes("items/item/consumable")?[i].InnerXml),
                    isRecoverable = Convert.ToBoolean(xmlDocument.SelectNodes("items/item/recoverable")?[i].InnerXml),
                    pathToImage = xmlDocument.SelectNodes("items/item/path")?[i].InnerXml,
                    count = Convert.ToInt32(xmlDocument.SelectNodes("items/item/count")?[i].InnerXml),
                    cost = Convert.ToInt32(xmlDocument.SelectNodes("items/item/cost")?[i].InnerXml),
                    description = xmlDocument.SelectNodes("items/item/description")?[i].InnerXml
                };
                _items.Add(item);
            }
        }

        //Поиск конкретного предмета в общем списке
        public Item FindItemInList(string itemName)
        {
            Item item = _items.Find( x => x.name==itemName);
            return item;
        }

        //Для добавленя предмета в список предметов, которые есть у игрока
        public void AddItem(string itemKey, int itemValue)
        {
            try
            {
                Items.Add(itemKey, itemValue);
            }
            catch (ArgumentException)
            {
                Items[itemKey] += itemValue;
            }
        }

        //Удаляем предметы из инвентаря
        public void RemoveItem(string itemName, int count)
        {
            if(Items[itemName] - count <= 0)
                Items.Remove(itemName);
            else
            {
                Items[itemName] = Items[itemName] - count;
            }
        }
    
        //Вызывается когда мы открываем инвентарь
        public void LoadInventory(RectTransform content, string path)
        {
            var count = 0;
        
            //Очищаем инвентарь 
            foreach (Transform child in content.transform) {
                Destroy(child.gameObject);
            }
        
            //Отрисовываем предметы в инвентаре
            foreach (var key in Items.Keys)
            {
                var instance = Instantiate(Resources.Load(path)) as GameObject;
                if (instance != null)
                {
                    instance.name = key;
                    instance.transform.SetParent(content, false);
                    Item item = FindItemInList(key);
                    InitializeItem(instance, item, Items[key]);
                }
                count++;
            }
            //Если по итогу предметов больше 12 то выходим
            if(count > 16)
                return;
            //Если меньше, то добавляем  пустые слоты, для красоты
            for (int i = count; i < 16; i++)
            {
                var instantiate = Instantiate(Resources.Load("UI/InventoryItemSlot"), content, false) as GameObject;
            }
        }

        public void LoadFastSlotInInventory(GameObject[] img)
        {
            //Очищаем быстрые слоты в инвентаре
            foreach (var i in img)
            {
                i.GetComponent<Image>().sprite = null;
                i.GetComponent<CanvasGroup>().alpha = 0;
                i.GetComponent<CanvasGroup>().blocksRaycasts = false;
                i.GetComponent<CanvasGroup>().interactable = false;
            }
        
            //Отрисовываем пердметы в быстрых слотах, если они есть
            foreach (var key in SlotItem.Keys)
            {
                Item item = FindItemInList(SlotItem[key]);
                Sprite sp  = Resources.Load<Sprite>(item.pathToImage);
                img[key].GetComponent<Image>().sprite = sp;
            
                img[key].GetComponent<CanvasGroup>().alpha = 1;
                img[key].GetComponent<CanvasGroup>().blocksRaycasts = true;
                img[key].GetComponent<CanvasGroup>().interactable = true;
            }  
        }

        private void InitializeItem(GameObject itemPrefab, Item it, int itemCount)
        {
            itemPrefab.GetComponent<InventoryElement>().Element = InventoryElement.CurrentElement.PlayerInventory;
            Sprite sp  = Resources.Load<Sprite>(it.pathToImage); 
            itemPrefab.transform.GetChild(0).GetComponent<Image>().sprite = sp;
        
            itemPrefab.GetComponent<Button>().onClick.AddListener(ItemClick);
        
            itemPrefab.transform.GetChild(0).GetComponent<CanvasGroup>().alpha = 1;
            itemPrefab.transform.GetChild(0).GetComponent<CanvasGroup>().blocksRaycasts = true;
            itemPrefab.transform.GetChild(0).GetComponent<CanvasGroup>().interactable = true;
       
            itemPrefab.transform.GetChild(1).GetComponent<Text>().text = itemCount.ToString();
        }

        //Нажатие на предмет в инвентаре
        private void ItemClick()
        {
            //Если у нас не выбран слот для добавления предмета то выходим
            if (!isSlotHighlighted) return;
            //Отправляем сообщение для добавления предмета в быстрый слот
            currentItem = EventSystem.current.currentSelectedGameObject.name;
            GameObject[] obj = GameObject.FindGameObjectsWithTag("Slot");
            foreach (var o in obj)
            {
                if (o.GetComponent<FastSlotItemScript>().slotNumber == currentSlot)
                {
                    o.GetComponent<FastSlotItemScript>().SetItemToSlot();
                }
            }
        }
    
        //Добавляем предмет в выбранный быстрый слот
        public void AddItemToSlot(int slotNumber, string itemName, Image inventoryFastSlot, Image itemImage, Text itemCountText)
        {
            Item item = FindItemInList(itemName);
            if(!item.isConsumable)
                return;
            try
            {
                SlotItem.Add(slotNumber, itemName);
            }
            catch (ArgumentException)
            {
                SlotItem[slotNumber] = itemName;
            }
            InitializeItemInFastSlot(itemName, inventoryFastSlot, itemImage, itemCountText);
            Managers.Data.SavePlayerInventory();
        }

        private void InitializeItemInFastSlot(string itemName, Image inventoryFastSlot, Image itemImage, Text itemCountText)
        {
            Sprite sp  = Resources.Load<Sprite>("Images/Items/" + itemName);
            itemImage.sprite = sp;

            inventoryFastSlot.sprite = sp;
            inventoryFastSlot.GetComponent<CanvasGroup>().alpha = 1;
            inventoryFastSlot.GetComponent<CanvasGroup>().blocksRaycasts = true;
            inventoryFastSlot.GetComponent<CanvasGroup>().interactable = true;
        
            itemImage.GetComponent<CanvasGroup>().alpha = 1;
            itemImage.GetComponent<CanvasGroup>().blocksRaycasts = true;
            itemImage.GetComponent<CanvasGroup>().interactable = true;
        
            itemCountText.text = Items[itemName].ToString();
        }
    
        public void RemoveItemFromFastSlot(int slotNumber, Image inventoryItemImage, Image itemImage, Text itemCount)
        {
            SlotItem.Remove(slotNumber);
        
            inventoryItemImage.sprite = null;
            inventoryItemImage.GetComponent<CanvasGroup>().alpha = 0;
            inventoryItemImage.GetComponent<CanvasGroup>().blocksRaycasts = false;
            inventoryItemImage.GetComponent<CanvasGroup>().interactable = false;
        
            itemImage.sprite = null;
            itemImage.GetComponent<CanvasGroup>().alpha = 0;
            itemImage.GetComponent<CanvasGroup>().blocksRaycasts = false;
            itemImage.GetComponent<CanvasGroup>().interactable = false;
            itemCount.text = "";
        }
        public void UseItemFromSlot(int slotNumber, Text itemCountText)
        {
            if (!SlotItem.ContainsKey(slotNumber))
            {
                Debug.Log("Slot is empty");
                return;
            }
        
            string itemName = SlotItem[slotNumber];

            if(Items[itemName] == 0)
                return;
        
            Items[itemName] -= 1;
            itemCountText.text = Items[itemName].ToString();
        
            var it = FindItemInList(itemName);
            Item.GetItemEffect(it.itemType, it.name);
            Managers.Data.SavePlayerInventory();
        
            //Если после применения количество равно нулю и он не востанавливаемый
            //То удаляем его
            if (Items[itemName] != 0 || it.isRecoverable) return;
            var itemSlot = FindSlot(slotNumber);
            Items.Remove(itemName);
            RemoveItemFromFastSlot(slotNumber, itemSlot.transform.GetChild(1).GetComponent<Image>(),
                itemSlot.transform.GetChild(1).GetComponent<Image>(),itemSlot.transform.GetChild(0).GetComponent<Text>());
        }
    
        public int RestoreRecoverableItem(string itemName, int count)
        {
            //Обновляем количество предмета
            Items[itemName] = count;
            //Определяем, занимает ли предмет быстрый слот
            //Если да, то возвращяем номер слота
            foreach (var slot in SlotItem.Where(slot => slot.Value == itemName))
            {
                return slot.Key;
            }
            return 5;
        }
    
        //Поиск объекта быстрого слота, по его номеру
        private GameObject FindSlot(int slotNumber)
        {
            GameObject slot = null;
            slot = GameObject.FindWithTag("Slot" + slotNumber);
            return slot;
        }

        public void UpdateData(Dictionary<string,int> Items, Dictionary<int, string> SlotItem)
        {
            this.Items.Clear();
            this.Items = Items;
        
            this.SlotItem.Clear();
            this.SlotItem = SlotItem;
        
        }
    }
}
